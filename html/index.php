<!DOCTYPE html>
<html>
  <head>
    <title>Data operátorů</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>

    <!-- include cartodb.js library -->
    <script src="https://cartodb-libs.global.ssl.fastly.net/cartodb.js/v3/3.15/cartodb.js"></script>
    <link rel="stylesheet" href="https://cartodb-libs.global.ssl.fastly.net/cartodb.js/v3/3.15/themes/css/cartodb.css" />

    <!-- Statistics library -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/simple-statistics/1.0.1/simple_statistics.min.js"></script>


    <!-- slider -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/css/ion.rangeSlider.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/css/ion.rangeSlider.skinFlat.css" />
    <!-- <link rel="stylesheet" href="http://ionden.com/a/plugins/ion.rangeSlider/static/css/ion.rangeSlider.skinModern.css" /> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/js/ion.rangeSlider.js"></script>

    <!-- MDL -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/dialog-polyfill/0.4.2/dialog-polyfill.min.js"></script>



    <!-- Plot.ly -->
    <script type="application/javascript" src="https://cdn.plot.ly/plotly-latest.min.js"></script>


    <!-- Dialog -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dialog-polyfill/0.4.7/dialog-polyfill.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dialog-polyfill/0.4.7/dialog-polyfill.css" />



    <!-- https://codepen.io/rkchauhan/pen/ONJEYL -->
    <link rel="stylesheet" href="css/checkbox.css" />
    <script src="js/checkbox_color.js"></script>

    <!-- my css -->
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/slider.css" />
    <link rel="stylesheet" href="css/mdl.css" />
    <link rel="stylesheet" href="css/right_panel.css" />
    <link rel="stylesheet" href="css/dialog.css" />
    <link rel="stylesheet" href="css/loading.css" />

    <!-- my js -->
    <script src="js/main.js"></script>
    <script src="js/dialog.js"></script>
    <script src="js/ui.js"></script>
    <script src="js/detail.js"></script>
    <script src="js/drawer.js"></script>
    <script src="js/filter.js"></script>
    <script src="js/slider.js"></script>
    <script src="js/bins.js"></script>
    <script src="js/animate.js"></script>
    <script src="js/checkbox.js"></script>
    <script src="js/action_button.js"></script>
    <script src="js/right_panel.js"></script>
    <script src="js/graph.js"></script>
    <script src="js/loading.js"></script>
    <script src="js/change_project_button.js"></script>

  </head>

  <body>

    <?php include "html/google_analytics.php"; ?>

    <!-- Uses a transparent header that draws on top of the layout's background -->
  <div class="demo-layout-transparent mdl-layout mdl-js-layout">
    <header class="mdl-layout__header mdl-layout__header--transparent">
      <div class="mdl-layout__header-row">
        <!-- Title -->
        <span class="mdl-layout-title" id="page_title">Počet lidí</span>
        <!-- Add spacer, to align navigation to the right -->
        <div class="mdl-layout-spacer"></div>

        <!-- Checkbox Všední dny -->
        <div class="rkmd-checkbox checkbox-ripple checkbox-rotate checkbox-dark" style="display: inline-block; color: rgba(0,0,0,0.87);">
          <label class="input-checkbox checkbox-red"  id="checkbox-workday-div">
            <input type="checkbox" id="checkbox-workday" checked>
            <span class="checkbox"></span>
          </label>
          <label for="checkbox-workday" class="label">Pracovní den</label>

          <label class="input-checkbox checkbox-red"  id="checkbox-weekend-div">
            <input type="checkbox" id="checkbox-weekend">
            <span class="checkbox"></span>
          </label>
          <label for="checkbox-weekend" class="label">Víkend</label>
        </div>
      </div>
    </header>
    <div class="mdl-layout__drawer">
      <span class="mdl-layout-title">Data operátorů</span>
      <nav class="mdl-navigation mdl-layout-spacer">

        <a class="mdl-navigation__link" id="d_people_count" href="#">Počet lidí</a>
        <a class="mdl-navigation__link" id="d_people_diff" href="#">Změna počtu lidí</a>
        <hr>
        <a class="mdl-navigation__link" id="d_density_count" href="#">Hustota lidí</a>
        <a class="mdl-navigation__link" id="d_density_diff" href="#">Změna hustoty lidí</a>


        <div class="mdl-layout-spacer"></div>

        <a href="#" class="mdl-navigation__link" id="brno_jmk_button">Data pro kraj</a>
        <a href="#" class="mdl-navigation__link" id="button_dialog"> O projektu</a>
      </nav>
    </div>

  </div>
  <div id="map"></div>


  <?php include "html/slider.php"; ?>
  <?php include "html/action_button.php"; ?>
  <?php include "html/right_panel.php"; ?>
  <?php //include "html/checkbox.php"; ?>



    <script>
    MyMap = {};
    MyMap.jmk = false;
    MyMap.carto_layer = null;
    MyMap.api_key = "a50f67b019a990a1f29893ca6beab24028c2aea0";
    MyMap.filter_from = 15;
    MyMap.filter_to = 16;
    MyMap.filter_day = "workday";
    MyMap.bins = "qualnite";
    MyMap.filter_type = "static";
    MyMap.filter_variable = "users";
    MyMap.slider_timeout = null;
    MyMap.animation_timeout = null;
    MyMap.animation_running = false;


    if(window.location.hostname == "operator.brno.ml"){
        console.log("Website as operator.brno.ml");
        MyMap.jmk = false;
    }
    else{
        console.log("Website as jmk.brno.ml");
        MyMap.jmk = true;
    }

    window.onload = main;
    </script>

    <?php include "html/dialog.php" ?>

  </body>
</html>

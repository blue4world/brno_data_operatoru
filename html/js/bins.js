
function caluclateRangesQuantile(data){
  var values = [];
  data.rows.forEach(function(row,i) {
      if(row.buckets) {
          values.push(row.buckets);
      }
  });
  return values;
}

function caluclateRangesEqual(data){
  var values = [];
  data.rows.forEach(function(row,i) {
      if(row.users) {
          values.push(row.users);
      }
  });

  // create array of range clusters
  var clusters = ss.ckmeans(values, 10);

  // pull out the low and high values from each cluster
  var ranges = clusters.map(function(cluster){
      return [cluster[0],cluster.pop()];
  });

  outdata = []
  for(var i = 0; i < ranges.length; i++) {
    outdata.push(ranges[i][1]);
  }

  return outdata;
}

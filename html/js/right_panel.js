function showDetail(data,latlng){
      showRightPanel();
      setRightPanelName(data.nazev);
      if(MyMap.jmk){
        showLineGraph(data.kod_obec,data.pocet_obyvatel,data.plocha,data.nazev);
      }
      else{
        showLineGraph(data.kod_zsj,data.pocet_obyvatel,data.plocha,data.nazev);
      }

}

function setRightPanelName(name){
    $('#right_panel_address').html(''+name);
}

function setStandardTitleImage(){
  $('#right_panel_title').css("background","url('img/data_brno_logo.svg') 100% 90% no-repeat");
  $('#right_panel_title').css("height", "130px");
  $('#right_panel_title').css("background-position", "50% 50%");
  $('#right_panel_title').css("border", "inset 15px transparent");
  $('#right_panel_title').css("border-bottom", "inset 30px transparent");
  $('#right_panel_title').css("box-sizing", "border-box");
}



function showRightPanel(){
  showElement("#right_panel");
  setStandardTitleImage();
  $('#right_panel_address').html('');
  var content_html = '';
  /*
  content_html += '<h6 style="margin:0">Stoličky</h6><div id="pie_chart" class="graph_pie_div"></div>';
  content_html += '<br/><br/><h6 style="margin:0">Obsazenost <span id="visitors_count"></span></h6><div id="bar_chart" class="graph_bar_div"></div>';
  $("#right_panel_content").html(content_html);
  */

  content_html +=  '<div id="line_chart" class="graph"></div>';
  $("#right_panel_content").html(content_html);
}

function hideRightPanel(){
  hideElement('#right_panel');
}

function initRightPanel(){
  $("#right_panel").click(function (){
    event.stopPropagation();
  });

  $("#right_panel_close").click(function (){
      hideRightPanel();
  });
}

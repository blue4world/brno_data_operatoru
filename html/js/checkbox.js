function initCheckbox(){
  $("#checkbox-workday").on("click", function () {
      $( "#checkbox-weekend-div" ).trigger( "click" );

      if(MyMap.filter_day == "weekend"){
        MyMap.filter_day = "workday";
      }
      else{
          MyMap.filter_day = "weekend";
      }

      filterResultsThread();
  });
  $("#checkbox-weekend").on("click", function () {
      $( "#checkbox-workday-div" ).trigger( "click" );

      if(MyMap.filter_day == "workday"){
        MyMap.filter_day = "weekend";
      }
      else{
          MyMap.filter_day = "workday";
      }

      filterResultsThread();
  });

/*
  $("#checkbox-static").on("click", function () {
      $( "#checkbox-diff-div" ).trigger( "click" );

      if(MyMap.filter_type == "diff"){
          MyMap.filter_type = "static";
      }
      else{
          MyMap.filter_type = "diff";
      }

      filterResultsThread();
  });
  $("#checkbox-diff").on("click", function () {
      $( "#checkbox-static-div" ).trigger( "click" );

      if(MyMap.filter_type == "static"){
        MyMap.filter_type = "diff";
      }
      else{
          MyMap.filter_type = "static";
      }
      filterResultsThread();
  });
*/

}

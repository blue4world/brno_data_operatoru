function getLayout(){
  var data  = {
      showlegend: false,
      autosize: true,
      // width: 600,
      //height: 400,
      //yaxis: { tickformat: 'd' },
      //xaxis: { tickformat: 'd' },
      margin: {
          l: 50,
          r: 80,
          b: 50,
          t: 0
      }
    };
    if(MyMap.filter_variable == "users"){
      data['yaxis'] = { tickformat: 'd'  };
      data['xaxis'] = { tickformat: 'd'  };
    }
    else{
      data['yaxis'] = { tickformat: '~s' };
      data['xaxis'] = { tickformat: '~s' };
    }
    return data;
}


function showLineGraph(kod_zsj,pocet_obyvatel,plocha,nazev){
  var var_name = 'kod_zsj';
  var table = 'zsj_mobilni_operator_2019';
  if(MyMap.jmk){
      var_name = 'kod_obec_p';
      table = 'obce_jmk_mobilni_operator_2019';
  }

  var url = getSQLApiUrl()+encodeURI("select * from "+table+" where "+var_name+" = "+kod_zsj+" and day = '"+MyMap.filter_day+"'");
  var x_data = new Array();
  var y_data = new Array();

  $.getJSON(url, function(data) {
    for (var i in data["rows"]) {
        x_data.push(data["rows"][i]["hour"]+":00");
        if(MyMap.filter_variable == "users"){
          y_data.push(parseInt(data["rows"][i]["users"]));
        }
        else{
          if(MyMap.jmk){
            y_data.push(parseInt(parseInt(data["rows"][i]["users"])/parseFloat(plocha/1000000)));
          }
          else{
            y_data.push(parseInt(parseInt(data["rows"][i]["users"])/parseFloat(plocha)));
          }
        }
    }

    var trace_mobile = {
      x: x_data,
      y: y_data,
      mode: 'lines',
      name: '',
      text: 'Mobilní operátor',
      line: {
        color: 'rgb(0, 200, 0)',
        width: 1
      }
    };

    var count_value;
    if(MyMap.filter_variable == "users"){
      count_value = parseInt(pocet_obyvatel);
    }
    else{
      if(MyMap.jmk){
        count_value = parseInt(pocet_obyvatel/parseFloat(plocha/1000000));
      }
      else{
        count_value = parseInt(pocet_obyvatel/parseFloat(plocha));
      }
    }

    var trace_count = {
      x: x_data,
      y: getArrayWithValue(x_data.length,count_value),
      mode: 'lines',
      name: '',
      text: 'Stálí rezidenti (ČSÚ)',
      line: {
          color: 'rgb(200, 0, 0)',
          width: 1
      }
    };

    var image_data;
    Plotly.newPlot('line_chart', [trace_mobile, trace_count], getLayout(), {displayModeBar: false} )
    .then(
    function(gd)
     {
      Plotly.toImage(gd,{height:800,width:800})
         .then(
             function(url)
         {

            // img_jpg.attr("src", url);
             image_data = url;
             //console.log(url);
             return Plotly.toImage(gd,{format:'jpeg',height:400,width:400});
         }
         )
    });

    $("#right_panel_download_data").unbind('click');
    $("#right_panel_download_data").click(function (){

      // data
      let csvContent = "data:text/csv;charset=utf-8,";
      csvContent += 'čas' + ","+ 'hodnota' + "\r\n";
      for(var i=0;i<y_data.length;i++){
        csvContent += x_data[i] + ","+ y_data[i] + "\r\n";
      }
      var encodedUri = encodeURI(csvContent);
      var link = document.createElement("a");
      link.setAttribute("href", encodedUri);
      link.setAttribute("download", nazev+".csv");
      document.body.appendChild(link); // Required for FF
      link.click();


      // image
      var encodedUri = encodeURI(image_data);
      var link2 = document.createElement("a");
      link2.setAttribute("href", encodedUri);
      link2.setAttribute("download", nazev+".png");
      document.body.appendChild(link2); // Required for FF
      link2.click();
    });

  });
}


function getArrayWithValue(lenght,value){
  var array = [];
  for (var i = 1; i <= lenght; i++) {
    array.push(value);
  }
  return array;
}

function getJsonMobile(){

}

///// OBSOLETE FILE!!!!

// create layer selector
function createSelector(layer) {

  var $options = $('#layer_selector li');
  $options.click(function(e) {
    // get the area of the selected layer
    var $li = $(e.target);
    var area = $li.attr('data');

    // deselect all and select the clicked one
    $options.removeClass('selected');
    $li.addClass('selected');

    var from = 7;
    var to = 8;
    // create query based on data from the layer
    var sql_dynamic = `with
      mobilni_operator as
        (select kod_zsj, users from zsj_mobilni_operator where hour >= `+from+` and hour <=`+to+` and day = 'workday'),
      data as
        (select kod_zsj, sum(users) as users from mobilni_operator group by kod_zsj)`;

    var sql_map = sql_dynamic + `SELECT zsj.cartodb_id,the_geom,the_geom_webmercator,users FROM zsj left join data on zsj.kod_zsj = data.kod_zsj`;

    var sql_legend = sql_dynamic + `SELECT users from data`;
    // change the query in the layer to update the map


    // Dynamic colors in the map -> https://gist.github.com/rgdonohue/d21239a488b5ab15dbbdf7567db1b086
    var url = "http://cartodb.ml/user/blue4world/api/v2/sql?format=JSON&api_key=a50f67b019a990a1f29893ca6beab24028c2aea0&q="+encodeURI(sql_legend);
    console.log(url);

    // request the (non-geometry) data first to calculate class ranges
    $.getJSON(url, function(data) {

        console.log(data);
        // push all the values into an array
        var values = [];
        data.rows.forEach(function(row,i) {
            if(row.users) {
                values.push(row.users);
            }
        });

        // create array of range clusters
        var clusters = ss.ckmeans(values, 10);

        // pull out the low and high values from each cluster
        var ranges = clusters.map(function(cluster){
            return [cluster[0],cluster.pop()];
        });

        makeMap(ranges,layer,sql_map);
    });

});
}

  function makeMap(ranges,layer,sql_map) {

    var colors = ['#eff3ff','#c6dbef','#9ecae1','#6baed6', '#4292c6','#2171b5','#084594'];
    var opacity = ["0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1.0"];

    // build the cartoCSS
    var cartoCSSRules = `
#zsj{
  polygon-fill: #d7191c;
  polygon-opacity: 0.1;
  line-color: #FFF;
  line-width: 0;
  line-opacity: 0;
}  `;
    // loop backwards, high to low
    for(var i = ranges.length; i--;) {
        var value = ranges[i][1],
            color = opacity[i];
        // build the individual rules
        var thisRule = '#zsj [ users <= ' +
            value + '] {    polygon-fill: #d7191c; polygon-opacity: '+color+' ;}  ';
        // add them to the cartoCSS
        cartoCSSRules += thisRule;
    }

    console.log(cartoCSSRules);

    layer.setSQL(sql_map);
    layer.setCartoCSS(cartoCSSRules);
  }

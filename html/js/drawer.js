function initDrawer(){
  $("#d_people_count").on("click", function () {
      MyMap.filter_type = "static";
      MyMap.filter_variable = "users";
      changeTitle($(this).html());
      filterResultsThread();
  });
  $("#d_people_diff").on("click", function () {
      MyMap.filter_type = "diff";
      MyMap.filter_variable = "users";
      changeTitle($(this).html());
      filterResultsThread();
  });
  $("#d_density_count").on("click", function () {
      MyMap.filter_type = "static";
      MyMap.filter_variable = "density";
      changeTitle($(this).html());
      filterResultsThread();
  });
  $("#d_density_diff").on("click", function () {
      MyMap.filter_type = "diff";
      MyMap.filter_variable = "density";
      changeTitle($(this).html());
      filterResultsThread();
  });
}

function runMapAnimation(){
  showStopIcon();
  MyMap.filter_from = -1;
  MyMap.filter_to = 0;
  //setSliderValue(MyMap.filter_from,MyMap.filter_to);

  var animate = function(){
    if(MyMap.filter_to < 24){
      MyMap.animation_running = true;
      MyMap.filter_from += 1;
      MyMap.filter_to += 1;
      console.log("Showing: "+MyMap.filter_from+" to "+MyMap.filter_to);
      setSliderValue(MyMap.filter_from,MyMap.filter_to);
      filterResultsThread();
      MyMap.animation_timeout = setTimeout(function(){ animate(); },4000);
    }
    else{
      MyMap.animation_running = false;
      showPlayIcon();
    }
  };
  animate();
}



function stopMapAnimation(){
  if(MyMap.animation_timeout){
    clearTimeout(MyMap.animation_timeout);
  }
  MyMap.animation_running = false;
  showPlayIcon();
}

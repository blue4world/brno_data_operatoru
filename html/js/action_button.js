function initActionButton(){

$("#action_button").on("click", function () {
      if(MyMap.animation_running == false){
          runMapAnimation();
      }
      else{
          stopMapAnimation();
      }
      });
}

function showPlayIcon(){
  $("#icon_play").css("display","block");
  $("#icon_stop").css("display","none");
}

function showStopIcon(){
  $("#icon_play").css("display","none");
  $("#icon_stop").css("display","block");
}

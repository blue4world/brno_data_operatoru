function main() {
  initChangeProjectButton();
  showLoading();
  initSlider();
  initCheckbox();
  initActionButton();
  initDrawer();
  initRightPanel();
  initDialogButton();

  var vis_url = 'http://cartodb.ml/user/blue4world/api/v2/viz/56aa56b2-b69c-11e7-b685-000000000000/viz.json';
  var zoom = 11;
  if(MyMap.jmk){
      vis_url = 'http://cartodb.ml/user/blue4world/api/v2/viz/90bbb780-5d5b-11e9-93df-000000000000/viz.json';
      zoom = 9;
  }

  cartodb.createVis('map', vis_url, {
    tiles_loader: true,
    center_lat: 49.195323,
    center_lon: 16.607889,
    zoom: zoom
  })
  .done(function(vis, layers) {



    // layer 0 is the base layer, layer 1 is cartodb layer
    var subLayer = layers[1].getSubLayer(0);
    MyMap.carto_layer = subLayer;

    subLayer.setInteraction(true);
    subLayer.on('featureClick', function(event, latlng, pos, data){
      console.log("Click on: "+data.nazev);
        if( $(document).width() > 600) {
          showDetail(data,latlng);
        }
        else{
          console.log("Screen is too small for graph");
        }
    });

    /*
    initSlider();
    initCheckbox();
    initActionButton();
    initDrawer();
    initRightPanel();
    initDialogButton();
    */

    filterResults();

    hideLoading();

    //runMapAnimation();
  })
  .error(function(err) {
    console.log(err);
  });
}

function changeTitle(title){
  $('#page_title').html(title);
}

function hideElement(id){
  $(id).css({ 'display': 'none' });
}

function showElement(id){
  $(id).css({ 'display': 'block' });
}

function initSlider(){
    $("#slider").ionRangeSlider({
        type: "double",
        grid: true,
        from_fixed: false,
        from: 9,
        to: 10,
        prettify_enabled: false,
        min_interval: 1,
        max_interval: 1,
        values: ["00:00","1:00","2:00","3:00","4:00","5:00","6:00","7:00","8:00","9:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00","23:00","24:00"],
        step: 1,
        drag_interval: true
    });


    $("#slider").on("change", function () {
      if(MyMap.animation_running == false){
        var $this = $(this),
        value = $this.prop("value").split(";");
        MyMap.filter_from = getNumberFromSlider(value[0]);
        MyMap.filter_to = getNumberFromSlider(value[1]);
        console.log("New slider value from "+getFilterFrom()+" to "+getFilterTo());
        filterResults();
      }
    });
}

function getFilterTo(){
  if(MyMap.filter_type == "diff"){
    if(MyMap.filter_to == 24){
        return 0;
    }
    else{
      return parseInt(MyMap.filter_to);
    }
  }
  else{
    /*
    if(MyMap.filter_to == 24){
      return 23;
    }
    */
    return parseInt(MyMap.filter_to)-1;
  }
}

function getFilterFrom(){
  if(MyMap.filter_type == "diff"){
      return parseInt(MyMap.filter_from);
  }
  else{
      return parseInt(MyMap.filter_from);
  }
}

function getNumberFromSlider(input){
    return parseInt(input.split(":")[0]);
}

function setSliderValue(from,to){
  var slider = $("#slider").data("ionRangeSlider");
  slider.update({
        from: from,
        to: to
    });
}

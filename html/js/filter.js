function getDynamicSQLBrno(){
  if(MyMap.filter_type == "diff"){
      return `with
          mobilni_operator_from as
            (select kod_zsj, users from zsj_mobilni_operator_2019 where hour = `+getFilterFrom()+` and day = '`+MyMap.filter_day+`'),
          mobilni_operator_to as
            (select kod_zsj, users from zsj_mobilni_operator_2019 where hour = `+getFilterTo()+` and day = '`+MyMap.filter_day+`'),
          data as
            (select mobilni_operator_from.kod_zsj, ROUND((mobilni_operator_to.users - mobilni_operator_from.users)::numeric,0) as users from mobilni_operator_from right join mobilni_operator_to on mobilni_operator_from.kod_zsj = mobilni_operator_to.kod_zsj)  `;
  }
  else{
        return `with
          mobilni_operator as
            (select kod_zsj, users from zsj_mobilni_operator_2019 where hour >= `+getFilterFrom()+` and hour <=`+getFilterTo()+` and day = '`+MyMap.filter_day+`'),
          data as
            (select kod_zsj as kod_zsj, ROUND((avg(users))::numeric,0) as users from mobilni_operator group by kod_zsj)  `;
    }
}

function getDynamicSQLJmk(){
  if(MyMap.filter_type == "diff"){
      return `with
          mobilni_operator_from as
            (select kod_obec_p, users from obce_jmk_mobilni_operator_2019 where hour = `+getFilterFrom()+` and day = '`+MyMap.filter_day+`'),
          mobilni_operator_to as
            (select kod_obec_p, users from obce_jmk_mobilni_operator_2019 where hour = `+getFilterTo()+` and day = '`+MyMap.filter_day+`'),
          data as
            (select mobilni_operator_from.kod_obec_p, ROUND((mobilni_operator_to.users - mobilni_operator_from.users)::numeric,0) as users from mobilni_operator_from right join mobilni_operator_to on mobilni_operator_from.kod_obec_p = mobilni_operator_to.kod_obec_p)  `;
  }
  else{
        return `with
          mobilni_operator as
            (select kod_obec_p, users from obce_jmk_mobilni_operator_2019 where hour >= `+getFilterFrom()+` and hour <=`+getFilterTo()+` and day = '`+MyMap.filter_day+`'),
          data as
            (select kod_obec_p as kod_obec_p, ROUND((avg(users))::numeric,0) as users from mobilni_operator group by kod_obec_p)  `;
    }
}


function getDynamicSQL(){
  if(MyMap.jmk){
    return getDynamicSQLJmk();
  }
  else{
    return getDynamicSQLBrno();
  }
}

function getMapSQLBrno(){
  return getDynamicSQL() + `SELECT zsj.cartodb_id,zsj.kod_zsj,pocet_ob_1 as pocet_obyvatel, plocha, the_geom,nazev,the_geom_webmercator,users,ROUND((users::float/plocha)::numeric,0) as density FROM zsj right join data on zsj.kod_zsj = data.kod_zsj where users > 10 or users < -10 `;
}

function getMapSQLJmk(){
  return getDynamicSQL() + `SELECT obce_jmk.cartodb_id, obce_jmk.kod_obec,pocet_ob_1 as pocet_obyvatel, plocha, the_geom, naz_obec as nazev, the_geom_webmercator,users,ROUND((users::float/(plocha/1000000))::numeric,0) as density FROM obce_jmk left join data on obce_jmk.kod_obec = data.kod_obec_p where users > 10 or users < -10 `;
}
function getMapSQL(){
  if(MyMap.jmk){
    return getMapSQLJmk();
  }
  else{
    return getMapSQLBrno();
  }
}

function getSQLApiUrl(){
  return "http://cartodb.ml/user/blue4world/api/v2/sql?format=JSON&api_key=a50f67b019a990a1f29893ca6beab24028c2aea0&q=";
}

function filterResults(){
  if(MyMap.slider_timeout){
    clearTimeout(MyMap.slider_timeout);
  }
  MyMap.slider_timeout = setTimeout(function(){ filterResultsThread(); }, 100);
}


function setFilterToCartoLayer(ranges){
  var css = generateCSS(ranges);
  var sql = getMapSQL();
  MyMap.carto_layer.setSQL(sql);
  MyMap.carto_layer.setCartoCSS(css);
}



function filterResultsThread(){
  if(MyMap.bins == "equal"){
    var url = getSQLApiUrl()+encodeURI(getDynamicSQL() + `SELECT `+MyMap.filter_variable+` from data`);
    $.getJSON(url, function(data) {
        var ranges = caluclateRangesEqual(data);
        setFilterToCartoLayer(ranges);
    });
  }
  else{
    var url = getSQLApiUrl()+encodeURI("select unnest(CDB_QuantileBins(array_agg(distinct(("+MyMap.filter_variable+"::numeric))), 10)) as buckets from ("+getMapSQL()+")_table_sql where "+MyMap.filter_variable+" is not null");
    $.getJSON(url, function(data) {
      var ranges = caluclateRangesQuantile(data);
      setFilterToCartoLayer(ranges);
    });
  }

}



function generateCSS(ranges){
  var css_variable = "zsj";
  if(MyMap.jmk){
    css_variable = "obce_jmk";
  }

  // build the cartoCSS
  var cartoCSSRules = `
                        #`+css_variable+`{
                          polygon-fill: #d7191c;
                          polygon-opacity: 0.1;
                          line-color: #fff;
                          line-width: 0.5;
                          line-opacity: 1;
                        }  `;

  var plusPos = findPositionOfGraterThanZero(ranges);

  // loop backwards, high to low
  for(var i = ranges.length-1; i >= 0 ;i--) {
      var value = ranges[i];
      var color_fill = "#d7191c";
      var color;
      var thisRule;

      if(MyMap.filter_type == "diff"){
              if(i == plusPos-1){
                cartoCSSRules += ' #'+css_variable+' [ '+MyMap.filter_variable+' = 0 ] {    polygon-fill: #ffffff; polygon-opacity: 0.5 ;}  ';
                cartoCSSRules += ' #'+css_variable+' [ '+MyMap.filter_variable+' < 0 ] {    polygon-fill: #d7191c; polygon-opacity: 0.1 ;}  ';
              }
              if(i >= plusPos){
                color_fill = "#1cd719";
                //color = (0.1*i)-((plusPos-2)*0.1);
                color = (0.1*i)+0.1;
              }
              else{
                color_fill = "#d7191c";
                color = (1.0)-(0.1*i);
              }
    }
    else{
      color = (i*0.1)+0.1;
    }

    thisRule = ' #'+css_variable+' [ '+MyMap.filter_variable+' <= ' + value + '] {    polygon-fill: '+color_fill+'; polygon-opacity: '+color+' ;}  ';
    // add them to the cartoCSS
    cartoCSSRules += thisRule;
  }

  // empty for no data
  // thisRule = ' #zsj [ '+MyMap.filter_variable+' <= ' + 20 + '] {  polygon-fill: #555555; polygon-opacity: 0.08 ;}  ';
  // cartoCSSRules += thisRule;


  //console.log("Css: "+cartoCSSRules);
  return cartoCSSRules;
}

function findPositionOfGraterThanZero(ranges){
  for(var i = 0; i < ranges.length ;i++) {
    if(ranges[i] >= 0){
      return i;
    }
  }
}


/*
function generateCSS(ranges){
      var opacity = ["0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1.0"];

      // build the cartoCSS
      var cartoCSSRules = `
                            #zsj{
                              polygon-fill: #d7191c;
                              polygon-opacity: 0.1;
                              line-color: #fff;
                              line-width: 0.5;
                              line-opacity: 1;
                            }  `;

      var plusPos = findPositionOfGraterThanZero(ranges);

      // loop backwards, high to low
      for(var i = ranges.length; i--;) {
          var value = ranges[i],
              color = opacity[i];
          // build the individual rules
          var color_fill = "#d7191c";
          var thisRule;

          // CSS for DIFF
          if(MyMap.filter_type == "diff"){
              // change colors
              if(value < 0){
                color_fill = "#d7191c";
              }
              else if(value == 0){
                color_fill = "#fffff";
              }
              else{
                color_fill = "#1cd719";
              }

              thisRule = '#zsj [ users <= ' +
                  value + '] {    polygon-fill: '+color_fill+'; polygon-opacity: '+color+' ;}  ';
          }
          // CSS for static
          else{
            thisRule = '#zsj [ users <= ' +
                value + '] {    polygon-fill: '+color_fill+'; polygon-opacity: '+color+' ;}  ';

          }
          // add them to the cartoCSS
          cartoCSSRules += thisRule;
      }

      console.log("Css: "+cartoCSSRules);
      return cartoCSSRules;
}
*/

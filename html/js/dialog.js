function initDialogButton(){
  var dialogButton = document.querySelector('#button_dialog');
  dialogButton.addEventListener('click', function() {
    // $('.mdl-layout__drawer').toggleClass('is-visible');
     dialog.showModal();
  });
  dialogSettings();
}

function dialogSettings(){
  var dialog = document.querySelector('#dialog');
  if (! dialog.showModal) {
    dialogPolyfill.registerDialog(dialog);
  }

  var dialogButtonClose = document.querySelector('#button_dialog_close');
  dialogButtonClose.addEventListener('click', function() {
    dialog.close();
  });
}

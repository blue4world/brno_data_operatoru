<div id="right_panel">
          <div class="right-card-wide mdl-card mdl-shadow--3dp">

          <div id="right_panel_address_div">
            <span id="right_panel_address"></span>
          </div>

            <div id="right_panel_title" class="mdl-card__title">
              <!-- <h2 class="mdl-card__title-text"></h2> -->
            </div>

            <div class="mdl-card__supporting-text">
              <div id="right_panel_content"></div>
            </div>

            <div class="mdl-card__menu">

              <!--
              <button id="right_panel_close" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
                <i class="material-icons">close</i>
              </button>
              -->

              <!-- Close -->
              <button id="right_panel_close" class="mdl-button mdl-js-button">
                <i class="material-icons">close</i>
              </button>

              <div class = "mdl-layout-spacer"></div>

              <!-- Download -->
              <button id="right_panel_download_data" class="mdl-button mdl-js-button">
                <i class="material-icons">cloud_download</i>
              </button>






            </div>
          </div>
    </div>


    <!-- action button -->
    <button id="action_button" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored">
      <i id="icon_play" class="material-icons">play_arrow</i>
      <i id="icon_stop" style="display: none;" class="material-icons">stop</i>
    </button>

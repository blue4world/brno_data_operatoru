<dialog id="dialog" class="mdl-dialog">
<h3 class="mdl-dialog__title">O projektu</h3>
<div class="mdl-dialog__content">

<p>
<a href="https://www.brno.cz/sprava-mesta/magistrat-mesta-brna/kancelar-namestka-primatora-pro-oblast-smart-city/oddeleni-dat-analyz-a-evaluaci/">Oddělení dat, analýz a evaluací Magistrátu města Brna </a>
představuje ve spolupráci se <a href="https://spatialhub.mendelu.cz/cs/">
SpatialHub </a> (Mendelova univerzita v Brně) aplikaci pro modelování počtu obyvatel během dne v Brně a obcích
Jihomoravského kraje.
</p>
<p>
Každé místo v metropolitní oblasti je charakteristické nejen svou polohou, ale také počtem obyvatel.
Počet obyvatel v území však není statické číslo, ale mění se v průběhu dne. V této aplikaci tak máte
možnost zjistit, kolik obyvatel se nachází v danou hodinu v daném místě. Aplikace využívá geolokační
data mobilního operátora T-mobile. Co jsou geolokační data? Jedná se o zbytková signalizační data,
které vysílá SIM karta do mobilní sítě (BTS). Na základě tohoto připojení pak lze určit místo, kde se
uživatel nachází. Jelikož má T-mobile cca 40% podíl na telekomunikačním trhu, jsou tato data
kalibrována na celou populaci (tedy do 100 %).
</p>


<p style="margin: 0;">
Při analýze se bere ohled na všechny buňky a všechny SIM v celé republice, díky čemuž automaticky
dochází ke kontrole a autokorekci možných anomálií (např. propad probingu kvůli problémům v síti).
Také dochází ke kontrole napříč dny, že není možné mít velké rozdíly jak mezi jednotlivými hodinami,
ale také např. ve dne a v noci. Též v případě velkých měst, jako je Brno, se přistupuje k ručnímu
ladění množství lidí oproti dat z ČSÚ. V zásadě se k množství lidí dle sčítání analýzou přičítají/odčítají
lidé pomocí dalších datasetů:
<ul style="margin: 0; padding-inline-start: 20px; ">
<li>
lokace pracovních míst na základě přílohy vyhlášky k zákonu o rozpočtovém určení daní
(zpracováno MF ČR)
</li>
<li>
evidence dlouhodobých pobytů cizích státních příslušníků (zdroj ČSÚ) na základě dat cizinecké
policie
</li>
</ul>
</p>


<p>
Pro místa, která nemají žádné trvalé bydlící, ale přesto jsou velmi frekventovaně navštěvované (např.
území FN Brno, Lužáneckého parku, apod.) se přikupují tzv. GPS body, díky nimž je možné data
mobilního operátora lépe kalibrovat (neplatí pro území kraje). Data byla zjišťována ve dvou po sobě
jdoucích týdnech - jejich výsledkem je tedy průměrný pracovní den (PO-PÁ) a průměrný víkendový
den (SO-NE).
</p>
<p>
  Zdrojové kódy projektu jsou dosutpné na <a href="https://bitbucket.org/blue4world/brno_data_operatoru"> bitbucket.org.  </a> <br/>
</p>

<!--
  <p>
    <a href="https://www.brno.cz/sprava-mesta/magistrat-mesta-brna/kancelar-namestka-primatora-pro-oblast-smart-city/oddeleni-dat-analyz-a-evaluaci/">Oddělení dat, analýz a evaluací Magistrátu města Brna </a>
      představuje ve spolupráci se
      <a href="https://spatialhub.mendelu.cz/cs/">
      SpatialHub (Mendelova univerzita v Brně)
      </a>
       aplikaci pro modelování počtu obyvatel během dne.
  </p>

  <p>
    Každé místo v metropolitní oblasti je charakteristické nejen svou polohou, ale také počtem obyvatel. Počet obyvatel v území však není statické číslo, ale mění se v průběhu dne. V této aplikaci tak máte možnost zjistit, kolik obyvatel se nachází v danou hodinu v daném místě. Aplikace využívá geolokalizační data mobilního operátora T-mobile.
    Co jsou geolokalizační data? Jedná se o zbytková signalizační data, které vysílá SIM karta do mobilní sítě (BTS). Na základě tohoto připojení pak lze určit místo, kde se uživatel nachází. Jelikož má T-mobile cca 40% podíl na telekomunikačním trhu, jsou tato data kalibrována na celou populaci (tedy do 100 %). Data jsou dále agregována (nelze zobrazit nižší počet obyvatel než 10), a to z důvodu ochrany údajů. Data byla zjišťována ve dvou po sobě jdoucích pracovních týdnech (ÚT-ČT), na základě kterých byl pak sestaven typický pracovní den. Totéž pak platí o stanovení typického nepracovního dne.
  </p>
  <p>
    Průměrný pracovní den vznikl průměrem následujících dní: <br/>
    20. - 22. září 2016 (úterý až čtvrtek) <br/>
    18. – 20. října 2016 (úterý až čtvrtek) <br/>
  </p>
  <p>
Průměrný pracovní den vznikl průměrem následujících dní: <br/>
24. září 2016 (sobota) <br/>
22. října 2016 (sobota) <br/>
  </p>
  <p>
    Zdrojové kódy projektu jsou dosutpné na <a href="https://bitbucket.org/blue4world/brno_data_operatoru"> bitbucket.org </a>
  </p>
-->
</div>

<div class="mdl-dialog__actions">
  <button id="button_dialog_close" type="button" class="mdl-button">Zavřít</button>
  <a href="mailto:data@brno.cz?subject=Dotaz - aplikace operator.brno.ml"> <button type="button" class="mdl-button">Mám dotaz</button></a>
</div>
</dialog>
